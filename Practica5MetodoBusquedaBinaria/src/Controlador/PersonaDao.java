/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.Lista.Lista;
import Modelo.Persona;

/**
 *
 * @author Jose Yangari
 */
public class PersonaDao extends AdaptadorDao<Persona> {

    private Persona persona;
    

    public PersonaDao() {
        super(Persona.class);
    }
    
    public Persona getPersona() {
        if (persona == null) 
            persona = new Persona(); 
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public boolean guardar(){
        persona.setId(new Long(Listar().tamanio()+1));
        return guardar(persona);
    }
    
    public Lista<Persona> Busqueda_Binaria(String dato, int tipo) {
        Lista aux = new Lista();
        Lista<Persona> lista = Listar();
        int central, inf, sup;
        Persona valorCentral;
        inf = 0;
        sup = lista.tamanio() - 1;
        while (inf <= sup) {
            central = (inf + sup) / 2;
            valorCentral = lista.consultarDatoPos(central);
            switch (tipo) {
                case 1:
                    if (valorCentral.getApellido().toUpperCase().contains(dato.toUpperCase())) {
                        aux.insertarDato(valorCentral);
                    }
                    if (valorCentral.toString().compareTo(dato.toString()) > 0) {
                        sup = central - 1;
                    } else {
                        inf = central + 1;
                    }
                    break;
                case 2:
                    if (valorCentral.getNombres().toUpperCase().contains(dato.toUpperCase())) {
                        aux.insertarDato(valorCentral);
                    }
                    if (valorCentral.toString().compareTo(dato.toString()) > 0) {
                        sup = central - 1;
                    } else {
                        inf = central + 1;
                    }
                    break;
                case 3:
                    if (valorCentral.getCedula().toUpperCase().contains(dato.toUpperCase())) {
                        aux.insertarDato(valorCentral);
                    }
                    if (valorCentral.toString().compareTo(dato.toString()) > 0) {
                        sup = central - 1;
                    } else {
                        inf = central + 1;
                    }
                    break;
            }
        }
        return aux;
    }
}
