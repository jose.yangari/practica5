/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Lista;

import java.io.Serializable;

/**
 *
 * @author sebas
 */
public class Nodo <T> implements Serializable{
    private T dato;
    private Nodo siguiente;

    public Nodo(T dato, Nodo nodo) {
        this.dato = dato;
        this.siguiente = nodo;
    }

    public Nodo() {
        this.dato = null;
        this.siguiente = null;
    }
       

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    
    
}
