/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Lista;

import Modelo.Persona;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 *
 * @author RODRIGO
 */
public class Lista<T> implements Serializable {

    private Nodo cabecera;
    private Class clazz;
    public static final Integer ASCENDENTE = 1;
    public static final Integer DESCENDENTE = 2;

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Lista() {
        this.cabecera = null;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {

        if (!estaVacio()) {
            Nodo tmp = cabecera;//
            while (tmp != null) {
                System.out.println(tmp.getDato());
                tmp = tmp.getSiguiente();
            }
        }
    }

    private void insertar(T dato) {
        Nodo tmp = new Nodo(dato, cabecera);
        cabecera = tmp;
    }

    public boolean insertarDato(T dato) {
        try {
            inserTarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public T consultarDatoPos(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            Nodo tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    public boolean modificarDatoPos(int pos, T data) {

        if (!estaVacio() && (pos <= tamanio() - 1)) {
            Nodo tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(data);
                return true;
            }
        }
        return false;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            Nodo tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            Nodo iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            Nodo tmp = new Nodo(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);
        }
    }

    private void inserTarFinal(T dato) {
        insertar(dato, tamanio() - 1);
    }

    private Field getField(String nombre) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.getName().equalsIgnoreCase(nombre)) {
                field.setAccessible(true);
                return field;
            }
        }
        return null;
    }

    /**
     * public void testReflect(T dato, String atributo) { try {
     * System.out.println(getField(atributo).get(dato).toString()); } catch
     * (Exception e) { System.out.println("Error"+e); } }*
     */
    private Object value(T dato, String atributo) throws Exception {
        return getField(atributo).get(dato);
    }

    //Ordenamiento por Metodo de seleccion
    public Lista<T> seleccion_clase(String atributo, Integer ordenacion) {
//        Lista<T> a=this;
        try {
            int i, j, k = 0;
            T t = null;
            int n = tamanio();

            for (i = 0; i < n - 1; i++) {
                k = i;
                t = consultarDatoPos(i);

                for (j = i + 1; j < n; j++) {
                    boolean band = false;
                    Object datoT = value(t, atributo);
                    Object datoj = value(consultarDatoPos(j), atributo);
                    if (datoT instanceof Number) {
                        Number aux = (Number) datoT;
                        Number numero = (Number) datoj;
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? numero.doubleValue() < aux.doubleValue()
                                : numero.doubleValue() > aux.doubleValue();
                        /*if (numero.doubleValue() < aux.doubleValue()) {
                            t = a.consultarDatoPos(j);
                            k = j;
                        }*/
                    } else {
                        band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                ? datoT.toString().compareTo(datoj.toString()) > 0
                                : datoT.toString().compareTo(datoj.toString()) < 0;
                        /*if (datoT.toString().compareTo(datoj.toString()) > 0) {
                            t = a.consultarDatoPos(j);
                            k = j;
                        }*/
                    }
                    if (band) {
                        t = consultarDatoPos(j);
                        k = j;
                    }
                }
                modificarDatoPos(k, consultarDatoPos(i));
                modificarDatoPos(i, t);
            }
        } catch (Exception e) {
            System.out.println("Error en ordenacion" + e);
        }
        return this;
    }

    public Lista<T> shellClase(String atributo, Integer ordenacion) {
        try {
            int salto, i;
            T t = null;
            T a = null;
            boolean cambios;

            for (salto = tamanio() / 2; salto != 0; salto /= 2) {
                cambios = true;

                while (cambios) {   // Mientras se intercambie algún elemento                                         
                    cambios = false;
                    for (i = salto; i < tamanio(); i++) // se da una pasada
                    {
                        t = consultarDatoPos(i);
                        boolean band = false;
                        Object datoT = value(t, atributo);
                        Object datoj = value(consultarDatoPos(i - salto), atributo);
                        if (t instanceof Number) {
                            Number aux = (Number) datoT;
                            Number numero = (Number) datoj;
                            band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                    ? numero.doubleValue() > aux.doubleValue()
                                    : numero.doubleValue() < aux.doubleValue();
                        } else {
                            band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                                    ? datoT.toString().compareTo(datoj.toString()) < 0
                                    : datoT.toString().compareTo(datoj.toString()) > 0;
                        }
                        if (band) {
//                            arreglo[i - salto] > arreglo[i]
                            a = consultarDatoPos(i);
                            modificarDatoPos(i, consultarDatoPos(i - salto));
                            modificarDatoPos(i - salto, t);
                            cambios = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error en ordenacion" + e);
        }
        return this;
    }

    public Lista<T> quickSort(String atributo, Integer ordenacion, Integer izq, Integer der) {
        try {
            int i, ult, m;
            T tmp = null;
            T t = null;
            if (izq >= der) {
                return this;
            }
            tmp = consultarDatoPos(izq);
            m = (izq + der) / 2;
            modificarDatoPos(izq, consultarDatoPos(m));
            modificarDatoPos(m, tmp);
            ult = izq;
            for (i = izq + 1; i <= der; i++) {
                t = consultarDatoPos(i);
                boolean band = false;
                Object datoT = value(tmp, atributo);
                Object datoj = value(t, atributo);
                if (tmp instanceof Number) {
                    Number aux = (Number) datoT;
                    Number numero = (Number) datoj;
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? numero.doubleValue() < aux.doubleValue()
                            : numero.doubleValue() > aux.doubleValue();
                } else {
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? datoT.toString().compareTo(datoj.toString()) > 0
                            : datoT.toString().compareTo(datoj.toString()) < 0;
                }
                if (band) {
                    tmp = consultarDatoPos(++ult);
                    modificarDatoPos(ult, consultarDatoPos(i));
                    modificarDatoPos(i, tmp);
                }
                tmp = consultarDatoPos(izq);
                modificarDatoPos(izq, consultarDatoPos(ult));
                modificarDatoPos(ult, tmp);
                quickSort(atributo, ordenacion, izq, ult - 1);
                quickSort(atributo, ordenacion, ult + 1, der);
            }
        } catch (Exception e) {
            System.out.println("Error a la ordenacion" + e);
        }
        return this;
    }

    public Lista<T> QuicksortLista(String a, Integer ordenacion, Integer primero, Integer ultimo) {
        try {
            int i, j, central;
            T pivote = null;
            central = (primero + ultimo) / 2;
            pivote = consultarDatoPos(central);
            Lista tmp = null;
            i = primero;
            j = ultimo;
            T p, p1, p2;
            do {
                p = consultarDatoPos(i);
                p1 = consultarDatoPos(j);
                p2 = pivote;
                boolean band = false;
                Object datoT = value(p, a);
                Object datoj = value(p2, a);
                if (p instanceof Number) {
                    Number aux = (Number) datoT;
                    Number numero = (Number) datoj;
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? numero.doubleValue() < aux.doubleValue()
                            : numero.doubleValue() > aux.doubleValue();
                } else {
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? datoj.toString().compareTo(datoT.toString()) > 0
                            : p1.toString().compareTo(datoj.toString()) < 0;
                }
                while (pivote.toString().compareTo(consultarDatoPos(i).toString()) > 0) {
                    i++;
                }
                while (consultarDatoPos(j).toString().compareTo(pivote.toString()) > 0) {
                    j--;
                }
                if (i <= j) {
                    T aux = consultarDatoPos(i);
                    modificarDatoPos(i, consultarDatoPos(j));
                    modificarDatoPos(j, aux);
                    i++;
                    j--;
                }
            } while (i <= j);
            if (primero < j) {
                QuicksortLista(a, ordenacion, primero, j);
            }
            if (i < ultimo) {
                QuicksortLista(a, ordenacion, i, ultimo);
            }
        } catch (Exception e) {
        }

        return this;
    }

    public Lista<T> QuicksortLista2(String a, Integer ordenacion, Integer primero, Integer ultimo) {
        try {
            int i, j, central;
            T pivote = null;
            central = (primero + ultimo) / 2;
            pivote = consultarDatoPos(central);
            Lista tmp = null;
            i = primero;
            j = ultimo;
            T p, p1, p2;
            do {
                p = consultarDatoPos(i);
                p1 = consultarDatoPos(j);
                p2 = pivote;
                boolean band = false;
                Object datoT = value(p, a);
                Object datoj = value(p2, a);
                
                if (p instanceof Number) {
                    Number aux = (Number) datoT;
                    Number numero = (Number) datoj;
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? numero.doubleValue() < aux.doubleValue()
                            : numero.doubleValue() > aux.doubleValue();
                } else {
                    band = (ordenacion.intValue() == Lista.ASCENDENTE.intValue())
                            ? datoj.toString().compareTo(datoT.toString()) > 0
                            : datoj.toString().compareTo(datoT.toString()) < 0;
                }

                while (band) {
                    i++;
                }
                while (band) {
                    j--;
                }
                if (i <= j) {
                    T aux = consultarDatoPos(i);
                    modificarDatoPos(i, consultarDatoPos(j));
                    modificarDatoPos(j, aux);
                    i++;
                    j--;
                }
            } while (i <= j);
            if (primero < j) {
                QuicksortLista(a, ordenacion, primero, j);
            }
            if (i < ultimo) {
                QuicksortLista(a, ordenacion, i, ultimo);
            }
        } catch (Exception e) {
        }

        return this;
    }
}
