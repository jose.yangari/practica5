/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.Lista.Lista;

/**
 *
 * @author Jose Yangari
 */
public interface InterfazDao<T> {
    
    //4 Metodos genericos.
    public boolean guardar(T dato);
    public boolean modificar(T dato, int pos);
    public Lista<T> Listar();
    public T buscarId();
    
}
