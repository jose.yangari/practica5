/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Modelo;

import Controlador.Lista.Lista;
import Modelo.Persona;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Det-Pc
 */
public class ModeloTablaPersona extends AbstractTableModel{
   private Lista<Persona> lista = new Lista();

    public Lista<Persona> getLista() {
        return lista;
    }

    public void setLista(Lista<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Persona p = lista.consultarDatoPos(i);
         switch (i1) {
            case 0:return (i+1);
            case 1:return p.getCedula();
            case 2:return p.getApellido()+" "+p.getNombres();
            case 3:return p.getEdad();
            case 4:return p.getCorreo();
            case 5:return p.getDireccion();
            default:return null;
        }
    }

    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0:return "nro";
            case 1:return "Cedula";
            case 2:return "Persona";
            case 3:return "Edad";
            case 4:return "Correo";
            case 5:return "Dirreccion";
            default: return null;
        }
    }
}
